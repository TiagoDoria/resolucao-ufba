<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resolução UFBA</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form"  method="POST" action="{{ route('register') }}">

                            <fieldset>
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Nome</label>
                                  <div  class="col-md-12">

                                      <input id="name" type="text" placeholder="Nome" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('name'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div style="height=2px;"></div>
                                  <label for="password-confirm" class="col-md-6">E-mail</label>
                                  <div class="col-md-12">
                                      <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('email'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('register') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Matrícula</label>
                                  <div class="col-md-12">

                                      <input id="register" type="text" placeholder="Matrícula" class="form-control" name="register" value="{{ old('register') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('register'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('register') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('lattes') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Lattes</label>
                                  <div class="col-md-12">

                                      <input id="lattes" type="text" placeholder="Lattes" class="form-control" name="lattes" value="{{ old('lattes') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('lattes'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('lattes') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('siape') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Siape</label>
                                  <div class="col-md-12">

                                      <input id="siape" type="text" placeholder="Siape" class="form-control" name="siape" value="{{ old('siape') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('siape'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('siape') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('departament') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Departamento</label>
                                  <div class="col-md-12">

                                      <input id="siape" type="text" placeholder="Departamento" class="form-control" name="departament" value="{{ old('departament') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('departament'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('departament') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('institution') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Instituição</label>
                                  <div class="col-md-12">

                                      <input id="institution" type="text" placeholder="Instituição" class="form-control" name="institution" value="{{ old('institution') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('institution'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('institution') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group{{ $errors->has('date_admission_ufba') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-6">Data de admissão UFBA</label>
                                  <div class="col-md-12">

                                      <input id="institution" type="text" placeholder="Data de admissão Ufba" class="form-control" name="date_admission_ufba" value="{{ old('date_admission_ufba') }}" required autofocus>
                                      <br>
                                      @if ($errors->has('date_admission_ufba'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('date_admission_ufba') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                </div>
                                <label for="password-confirm" class="col-md-6">Senha</label>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                  <div class="col-md-12">
                                      <input id="password" placeholder="Senha" type="password" class="form-control" name="password" required>
                                      <br>
                                      @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                               </div>
                               <label for="password-confirm" class="col-md-4">Repetir senha</label>
                               <div class="col-md-12">

                                 <input id="password-confirm" placeholder="Repetir senha" type="password" class="form-control" name="password_confirmation" required>
                                 <br>
                               </div>
                               <br>
                                <div class="form-group">
                                    <div class="col-md-12 ">
                                        <button type="submit"  class="btn btn-lg btn-success btn-block">
                                            Registrar
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/dist/js/sb-admin-2.js"></script>

</body>

</html>
