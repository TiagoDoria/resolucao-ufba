<head>
@if($reports)
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resolução UFBA</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="css/relatorio.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <img id="img-ufba" src="img/ufba.jpg" width="200" height="230">
        </div>
        <br>
        <div class="col-md-12">
          <h3 id="title-ufba">Universidade Federal da Bahia</h3>
        </div>

        <br><br>
         <div class="col-md-12">
          <h3 id="title-relatorio">Relatório - Resolução UFBA</h3>
        </div>
        <br><br>
         <div class="col-md-12" id="dados">
          <h4><span style="font-weight: bolder;">Professor:</span> {{$reports->name}} </h4>
          <h4><span style="font-weight: bolder;">Matrícula:</span> {{$users->register}} </h4>
          <h4><span style="font-weight: bolder;">Departamento:</span> {{$users->departament}} </h4>
          <h4><span style="font-weight: bolder;">Instituição:</span> {{$users->institution}} </h4>
         <!--  <h4><span style="font-weight: bolder;">Atividade:</span> {{$reports->description}} </h4>
          <h4><span style="font-weight: bolder;">Campo:</span> {{$reports->field}} </h4>-->
        </div>
    </div>
    </div>
</body>
  

 
@endif
