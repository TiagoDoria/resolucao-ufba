<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resolução UFBA</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Custom CSS -->
    <link href="/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  
</head>

<body>

    <div id="wrapper">

      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">Resolução UFBA</a>
          </div>
          <!-- /.navbar-header -->

          <ul class="nav navbar-top-links navbar-right">
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-messages">
                      <li>
                          <a href="#">
                              <div>
                                  <strong>John Smith</strong>
                                  <span class="pull-right text-muted">
                                      <em>Yesterday</em>
                                  </span>
                              </div>
                              <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <strong>John Smith</strong>
                                  <span class="pull-right text-muted">
                                      <em>Yesterday</em>
                                  </span>
                              </div>
                              <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <strong>John Smith</strong>
                                  <span class="pull-right text-muted">
                                      <em>Yesterday</em>
                                  </span>
                              </div>
                              <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a class="text-center" href="#">
                              <strong>Read All Messages</strong>
                              <i class="fa fa-angle-right"></i>
                          </a>
                      </li>
                  </ul>
                  <!-- /.dropdown-messages -->
              </li>
              <!-- /.dropdown -->
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-tasks">
                      <li>
                          <a href="#">
                              <div>
                                  <p>
                                      <strong>Task 1</strong>
                                      <span class="pull-right text-muted">40% Complete</span>
                                  </p>
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                          <span class="sr-only">40% Complete (success)</span>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <p>
                                      <strong>Task 2</strong>
                                      <span class="pull-right text-muted">20% Complete</span>
                                  </p>
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                          <span class="sr-only">20% Complete</span>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <p>
                                      <strong>Task 3</strong>
                                      <span class="pull-right text-muted">60% Complete</span>
                                  </p>
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                          <span class="sr-only">60% Complete (warning)</span>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <p>
                                      <strong>Task 4</strong>
                                      <span class="pull-right text-muted">80% Complete</span>
                                  </p>
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                          <span class="sr-only">80% Complete (danger)</span>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a class="text-center" href="#">
                              <strong>See All Tasks</strong>
                              <i class="fa fa-angle-right"></i>
                          </a>
                      </li>
                  </ul>
                  <!-- /.dropdown-tasks -->
              </li>
              <!-- /.dropdown -->
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-alerts">
                      <li>
                          <a href="#">
                              <div>
                                  <i class="fa fa-comment fa-fw"></i> New Comment
                                  <span class="pull-right text-muted small">4 minutes ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                  <span class="pull-right text-muted small">12 minutes ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <i class="fa fa-envelope fa-fw"></i> Message Sent
                                  <span class="pull-right text-muted small">4 minutes ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <i class="fa fa-tasks fa-fw"></i> New Task
                                  <span class="pull-right text-muted small">4 minutes ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                              <div>
                                  <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                  <span class="pull-right text-muted small">4 minutes ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a class="text-center" href="#">
                              <strong>See All Alerts</strong>
                              <i class="fa fa-angle-right"></i>
                          </a>
                      </li>
                  </ul>
                  <!-- /.dropdown-alerts -->
              </li>
              <!-- /.dropdown -->
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  {{ Auth::user()->name }} <i class="fa fa-caret-down"></i>

                </a>
                  <ul class="dropdown-menu dropdown-user">
                      <li><a href="#"><i class="fa fa-user fa-fw"></i>Perfil</a>
                      </li>
                      <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                      </li>
                      <li class="divider"></li>
                      <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                      </li>
                  </ul>
                  <!-- /.dropdown-user -->
              </li>
              <!-- /.dropdown -->
          </ul>
          <!-- /.navbar-top-links -->

          <div class="navbar-default sidebar" role="navigation">
              <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">

                      <li>
                          <a href="/"><i class="fa fa-dashboard fa-fw"></i> Página inicial</a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Atividades<span class="fa arrow"></span></a>
                          <ul class="nav nav-second-level">
                              <li>
                                  <a href="/register_subactivity">Cadastrar</a>
                              </li>
                              <li>
                                  <a href="/my_activity">Visualizar</a>
                              </li>
                          </ul>
                          <!-- /.nav-second-level -->
                      </li>
                      <li>
                          <a href="tables.html"><i class="fa fa-table fa-fw"></i>Cadastrar pretensão</a>
                      </li>
                     


                  </ul>
              </div>
              <!-- /.sidebar-collapse -->
          </div>
          <!-- /.navbar-static-side -->
      </nav>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastrar atividade</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Cadastrar atividade</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form role="form" enctype="multipart/form-data" method="POST" files="true" action="{{ route('create_subactivity') }}">

                                            <fieldset>
                                                {{ csrf_field() }}

                                                 <label for="password-confirm" class="col-md-6">Campo</label>
                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                  <div class="col-md-12">
                                                    <div class="form-group">

                                                        <select name="field" class="form-group form-control" id="field"  required aria-required="true">
                                                        <option value="" >Escolha</option>
                                                        <option value="1">CAMPO I – ATIVIDADES DE ENSINO, ORIENTAÇÃO E PARTICIPAÇÃO EM BANCAS EXAMINADORAS</option>
                                                        <option value="2">CAMPO II - ATIVIDADES DE PESQUISA, PRODUÇÃO ACADÊMICA, CRIAÇÃO E INOVAÇÃO</option>
                                                        <option value="3">CAMPO III – ATIVIDADES DE EXTENSÃO</option>
                                                        <option value="4">CAMPO IV - RECEBIMENTO DE COMENDAS E PREMIAÇÕES ADVINDAS DO EXERCÍCIO DE ATIVIDADES ACADÊMICAS</option>
                                                        <option value="5">CAMPO V - PARTICIPAÇÃO EM ATIVIDADES EDITORIAIS E/OU DE ARBITRAGEM DE PRODUÇÃO INTELECTUAL E/OU ARTÍSTICA</option>
                                                        <option value="6">CAMPO VI- ATIVIDADES DE ADMINISTRAÇÃO/ REPRESENTAÇÃO/ ACADÊMICAS</option>
                                                        <option value="7">CAMPO VII - ATIVIDADES DE CAPACITAÇÃO PROFISSIONAL</option>
                                                        <option value="8">CAMPO VIII- ATIVIDADES PROFISSIONAIS</option>
                                                        <option value="9">CAMPO IX - AVALIAÇÃO DOCENTE PELOS DISCENTES</option>
                                                        </select>

                                                        @if ($errors->has('field'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('field') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                      <br>

                                                  </div>
                                               </div>
                                                   <label for="description" class="col-md-6">Descrição</label>
                                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                                  <div class="col-md-12">
                                                    <div class="form-group">

                                                        <select class="form-control" name="description" id="description" required aria-required="true">
                                                        <option value="" >Escolha</option>
                                                        
                                                        </select>

                                                        @if ($errors->has('description'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('description') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                      <br>

                                                  </div>
                                               </div>
                                              
                                               <div id="adicionais">
                                                
                                               </div>

                                               <div id="tembolsa">
                                               </div>
                                             

                                               

                                                <div class="form-group{{ $errors->has('anexo') ? ' has-error' : '' }}">
                                                  <label class="col-md-6">Anexo</label>
                                                  <div  class="col-md-12">

                                                      <input id="anexo" type="file"   name="anexo" value="{{ old('anexo') }}" required autofocus>
                                                      <br>
                                                      @if ($errors->has('anexo'))
                                                          <span class="help-block">
                                                              <strong>{{ $errors->first('anexo') }}</strong>
                                                          </span>
                                                      @endif
                                                  </div>
                                                </div>

                                                <div class="form-group col-md-12">
                                                  <label for="comment" style="font-weight:bolder;">Observação</label>
                                                  <textarea class="form-control" rows="5" id="observation" name="observation" ></textarea>
                                                </div>




                                               <div class="form-group">
                                                   <div class="col-md-12 ">
                                                       <button type="submit" name="upload" class="btn btn-lg btn-success btn-block">
                                                           Cadastrar
                                                       </button>
                                                   </div>
                                               </div>

                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        <!-- /.panel-footer -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
   <script>

      $('#field').on('change', function(e){
          console.log(e);
          var cat_id = e.target.value;
          var ctd = 1;
          var valor = [];
          $.get('/ajax-subcat?cat_id=' + cat_id, function(data){
              $('#description').empty();
              $.each(data, function(index, subcatObj){

                $('#description').append('<option value="'+subcatObj.description+'">'+subcatObj.description+'</option>');
                valor[ctd] = subcatObj.description;
                ctd++;
              });

                
              
          });  

         $('#description').on('change', function (x){
            var description2 = x.target.value;
          
            $.get('/ajax-subcat?description2=' + description2, function(data){
              $('#adicionais').empty();
                if(cat_id == 1){
                  if((description2 == valor[3]) || (description2 == valor[4]) || (description2 == valor[5]) ||
                    (description2 == valor[6]) || (description2 == valor[7]) || (description2 == valor[8]) ||
                    (description2 == valor[9]) || (description2 == valor[10]) || (description2 == valor[11])                    || (description2 == valor[12])|| (description2 == valor[13])|| (description2 == valor[14]))
                
                    $('#adicionais').append('@include('add_form.form1');');  
                

               }
            });
          });



      
      });

       $('#adicionais').on('change', function (y){
            var bolsa = y.target.value;
            $('#tembolsa').empty();
            if(bolsa == 'sim')
                $('#tembolsa').append('@include('add_form.form2');');
          });

</script>

    <!-- jQuery -->
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/vendor/raphael/raphael.min.js"></script>
    <script src="/vendor/morrisjs/morris.min.js"></script>
    <script src="/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/dist/js/sb-admin-2.js"></script>

</body>

</html>
