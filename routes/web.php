<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect('/login');
});

;

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/home2', function () {
        return view('/home2');
    });
    Route::get('/ajax-subcat', 'ActivityController@ajax_select')->name('ajax-subcat'); 


    Route::get('/register_subactivity', function () {
        return view('/create_activity');
    });
    Route::get('/pretensao', function () {
        return view('/pretension');
    });

    Route::post('/select_ajax', 'ActivityController@ajax_select')->name('select_ajax');

    Route::get('/download/{id}', 'TeacherController@download')->name('download');
    Route::get('/meuperfil', 'TeacherController@perfil')->name('meuperfil');
    Route::get('/view_details/{id}', 'TeacherController@view_detail')->name('view_details');
    Route::get('/download_edital/', 'ActivityController@download_edital')->name('download_edital');
    Route::get('/editar_perfil', 'TeacherController@edit')->name('editar_perfil');
    Route::post('/update', 'TeacherController@update')->name('update');
    Route::post('/create_pretension', 'TeacherController@create_pretension')->name('create_pretension');
    Route::post('/create_activity', 'ActivityController@register_activity')->name('create_activity');
    Route::post('/create_subactivity', 'TeacherController@register')->name('create_subactivity');
    Route::get('/my_activity', 'TeacherController@my_activity')->name('my_activity');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'auth.admin'])->group(function () {
  Route::get('/view_activity', 'ActivityController@view_activity')->name('view_activity');
  Route::get('/show_notifications', 'ActivityController@show_notifications')->name('show_notifications');
  Route::get('/acept_activity/{id}', 'ActivityController@acept_activity')->name('acept_activity');
  Route::get('/recuse_activity/{id}', 'ActivityController@recuse_activity')->name('recuse_activity');
  Route::get('/view/{id}', 'AdminController@view')->name('view');
  Route::get('/admin', function () {
      return view('admin');
  });
  Route::get('/show_activity', 'AdminController@show_activity')->name('show_activity');
  Route::get('/pendentes', 'AdminController@create_report')->name('create_report');
  Route::get('/analisados', 'AdminController@analisados')->name('analisados');
  Route::get('/download_file/{id}/{id2}/', 'AdminController@download_file')->name('download_file');
  Route::post('/send_comentary', 'AdminController@send_comentary')->name('send_comentary');
  Route::get('confirmar_relatorio/{id}/', 'AdminController@confirmar_relatorio')
  ->name('confirmar_relatorio');
  Route::get('/register_activity', function () {
        return view('admin.register_activity');
    });
  Route::get('/perfil_professor/{id}/', 'AdminController@perfil_professor')->name('perfil_professor');
  Route::get('/perfil', 'AdminController@perfil')->name('perfil');
  Route::get('/ver_relatorios', 'AdminController@ver_relatorios')->name('ver_relatorios');
  Route::get('create_report_pdf/{id}/', 'AdminController@create_report_pdf')->name('create_report_pdf');
});
