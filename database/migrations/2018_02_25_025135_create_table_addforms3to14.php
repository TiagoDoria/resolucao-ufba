<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAddforms3to14 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('c1_3to14', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_activity')->unsigned();
          $table->foreign('id_activity')->references('id')->on('register_activity');
          $table->string('student');
          $table->string('semester');
          $table->string('bolsa');
          $table->string('agencia');
          $table->string('titulo');

          $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
