-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Fev-2018 às 03:07
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resolucao_ufba`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `activity`
--

CREATE TABLE `activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` int(11) NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pontos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `activity`
--

INSERT INTO `activity` (`id`, `field`, `description`, `pontos`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ministrante de aulas em curso de graduação e em curso de pós-graduação stricto sensu (presenciais e EAD)', '1', '2018-02-12 16:11:15', '2018-02-12 16:11:15'),
(2, 1, 'Atividade de preceptoria/supervisão em curso de especialização (residência médica e multiprofissional)', '2', '2018-02-12 16:11:28', '2018-02-12 16:11:28'),
(3, 1, 'Supervisão de Pós-Doutorado concluída', '2', '2018-02-12 16:11:40', '2018-02-12 16:11:40'),
(4, 1, 'Orientação de Tese de Doutorado defendida', '5', '2018-02-12 16:11:54', '2018-02-12 16:11:54'),
(5, 1, 'Orientação de Dissertação de Mestrado defendida', '4', '2018-02-12 16:12:06', '2018-02-12 16:12:06'),
(6, 1, 'Coorientação de Tese de Doutorado defendida', '3', '2018-02-12 16:12:23', '2018-02-12 16:12:23'),
(7, 1, 'Coorientação de Dissertação de Mestrado defendida', '2', '2018-02-12 16:12:36', '2018-02-12 16:12:36'),
(8, 1, 'Orientação de Trabalho de Conclusão de Curso de Especialização concluída', '2', '2018-02-12 16:12:48', '2018-02-12 16:12:48'),
(9, 1, 'Orientação de Trabalho de Conclusão de Curso de Graduação', '2', '2018-02-12 16:12:58', '2018-02-12 16:12:58'),
(10, 1, 'Orientação de TCC em andamento (por estudante)', '1', '2018-02-12 16:13:12', '2018-02-12 16:13:12'),
(11, 1, 'Orientação de Tese em andamento (por estudante)', '3', '2018-02-12 16:13:26', '2018-02-12 16:13:26'),
(12, 1, 'Orientação de Dissertação de Mestrado em andamento (por estudante)', '2', '2018-02-12 16:13:37', '2018-02-12 16:13:37'),
(13, 1, 'Coorientação de Mestrado em andamento (por estudante)', '1', '2018-02-12 16:13:51', '2018-02-12 16:13:51'),
(14, 1, 'Coorientação de Doutorado em andamento (por estudante)', '1.5', '2018-02-12 16:14:06', '2018-02-12 16:14:06'),
(15, 1, 'Coordenação de programas institucionais (PET, PIBID, PROFICI, PIBIT e similares), por programa, mediante relatório atualizado', '3', '2018-02-12 16:14:19', '2018-02-12 16:14:19'),
(16, 1, 'Orientação em programas implantados na UFBA, aprovada pelo órgão de lotação do docente (Permanecer, PIBIC, PIBID, PIBITI, PIBIEX, ACCS, PET, Monitoria e similares), por estudante', '2', '2018-02-12 16:14:34', '2018-02-12 16:14:34'),
(17, 1, 'Supervisão de atividades práticas e estágios curriculares, obrigatórios e não obrigatórios (aluno da UFBA ou de outra instituição de ensino), por estudante', '1', '2018-02-12 16:14:46', '2018-02-12 16:14:46'),
(18, 1, 'Orientação acadêmica, oficializada de acordo com o Colegiado do Curso, por cada grupo de 10 estudantes', '1', '2018-02-12 16:14:57', '2018-02-12 16:14:57'),
(19, 1, 'Coordenação de disciplina, com relatórios homologados pelo órgão de lotação do docente', '2', '2018-02-12 16:15:11', '2018-02-12 16:15:11'),
(20, 1, 'Membro de Banca Examinadora de Livre Docência ou Tese do Doutorado', '4', '2018-02-12 16:15:22', '2018-02-12 16:15:22'),
(21, 1, 'Membro de Banca de Concurso Público para Professor da Carreira do Magistério Superior (professor efetivo)', '4', '2018-02-12 16:15:35', '2018-02-12 16:15:35'),
(22, 1, 'Membro de Banca Examinadora de Dissertação de Mestrado', '3', '2018-02-12 16:16:03', '2018-02-12 16:16:03'),
(23, 1, 'Membro de Banca Examinadora de Trabalhos de Conclusão de Curso de Especialização', '1', '2018-02-12 16:16:13', '2018-02-12 16:16:13'),
(24, 1, 'Membro de Banca Examinadora de Trabalhos de Conclusão de Curso de Graduação', '1', '2018-02-12 16:16:32', '2018-02-12 16:16:32'),
(25, 1, 'Membro de Banca de Seleção de Professor por tempo determinado', '1', '2018-02-12 16:16:44', '2018-02-12 16:16:44'),
(26, 1, 'Membro de Banca de Qualificação em cursos de pós-graduação', '1', '2018-02-12 16:16:56', '2018-02-12 16:16:56'),
(27, 1, 'Membro de Banca de Seleção para pós-graduação', '2', '2018-02-12 16:17:08', '2018-02-12 16:17:08'),
(28, 1, 'Membro de Banca de Seleção para bolsas institucionais', '1', '2018-02-12 16:17:18', '2018-02-12 16:17:18'),
(29, 2, 'Coordenação de projeto de pesquisa registrado na UFBA (por projeto, mediante relatório atualizado)', '3', '2018-02-12 16:27:31', '2018-02-12 16:27:31'),
(30, 2, 'Membro de grupo/projeto de pesquisa registrado na UFBA', '1', '2018-02-12 16:27:42', '2018-02-12 16:27:42'),
(31, 2, 'Elaboração de projetos de pesquisa aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '2', '2018-02-12 16:27:56', '2018-02-12 16:27:56'),
(32, 2, 'Elaboração de Relatórios de Pesquisa aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '4', '2018-02-12 16:28:07', '2018-02-12 16:28:07'),
(33, 2, 'Liderança de grupo de pesquisa da UFBA, conforme legislação desta (Resolução nº 02/2016 do CAPEX).', '5', '2018-02-12 16:28:19', '2018-02-12 16:28:19'),
(34, 2, 'Participação como conferencista ou palestrante em congressos, seminários, colóquios e outros eventos característicos da área de atuação do docente', '2', '2018-02-12 16:28:32', '2018-02-12 16:28:32'),
(35, 2, 'Ministrante de cursos (CH <8 h) em eventos acadêmicos', '02/atividade', '2018-02-12 16:28:44', '2018-02-12 16:28:44'),
(36, 2, 'Participação em eventos (congressos, simpósios, seminários, encontros etc.) na(s) área(s) de atuação do docente', '01/atividade', '2018-02-12 16:28:59', '2018-02-12 16:28:59'),
(37, 2, 'Artigo de pesquisa publicado em revista indexada, nacional ou internacional, na(s) área(s) de atuação do docente (impresso ou meio digital)', '15/publicação', '2018-02-12 16:29:14', '2018-02-12 16:29:14'),
(38, 2, 'Autoria de livro publicado (com ISBN), na(s) área(s) de atuação do docente, aprovado por Conselho Editorial, impresso ou meio digital', '25/publicação', '2018-02-12 16:30:23', '2018-02-12 16:30:23'),
(39, 2, 'Autoria de álbuns artísticos (CD, DVD ou formas equivalentes) especializado na área de atuação do docente', '25/publicação', '2018-02-12 16:30:35', '2018-02-12 16:30:35'),
(40, 2, 'Autoria de capítulo de livro publicado (com ISBN), na área de atuação do docente, aprovado por Conselho Editorial, impresso ou meio digital', '10/capítulo', '2018-02-12 16:30:45', '2018-02-12 16:30:45'),
(41, 2, 'Participação em álbuns artísticos na área de atuação do docente', '10/participação', '2018-02-12 16:30:59', '2018-02-12 16:30:59'),
(42, 2, 'Autoria de prefácio de livro, CD, DVD e mídias equivalentes', '02/publicação', '2018-02-12 16:31:14', '2018-02-12 16:31:14'),
(43, 2, 'Tradução de livro publicado (impresso ou meio digital)', '10/publicação', '2018-02-12 16:31:25', '2018-02-12 16:31:25'),
(44, 2, 'Tradução de capítulo de livro publicado (impresso ou meio digital)', '05/publicação', '2018-02-12 16:32:12', '2018-02-12 16:32:12'),
(45, 2, 'Tradução publicada de artigo (impresso ou meio digital)', '03/publicação', '2018-02-12 16:32:26', '2018-02-12 16:32:26'),
(46, 2, 'Publicação de trabalhos completos, de comunicação impressa ou meio digital, em anais de congressos, simpósios e similares, suplementos de periódicos ou cadernos especiais de jornais, na área de atuação do docente', '03/publicação', '2018-02-12 16:32:42', '2018-02-12 16:32:42'),
(47, 2, 'Resenha ou nota crítica publicada em revista indexada (impresso ou meio digital)', '02/publicação', '2018-02-12 16:33:17', '2018-02-12 16:33:17'),
(48, 2, 'Artigo publicado em jornal ou revista não indexada (impresso ou meio digital)', '02/publicação', '2018-02-12 16:33:30', '2018-02-12 16:33:30'),
(49, 2, 'Produção e publicação de material didático e hipertextos', '02/publicação', '2018-02-12 16:33:42', '2018-02-12 16:33:42'),
(50, 2, 'Produção de manual técnico', '02/publicação', '2018-02-12 16:33:54', '2018-02-12 16:33:54'),
(51, 2, 'Nota científica prévia', '01/publicação', '2018-02-12 16:34:04', '2018-02-12 16:34:04'),
(52, 2, 'Texto escrito para catálogo de exposições publicado por instituição pública ou privada (museus e galerias)', '02/publicação', '2018-02-12 16:34:15', '2018-02-12 16:34:15'),
(53, 2, 'Autoria de peça teatral, musical ou coreografia, roteiro de cinema, vídeo, rádio ou televisão, monumentos artísticos', '25/peça', '2018-02-12 16:34:28', '2018-02-12 16:34:28'),
(54, 2, 'Direção de peças teatrais apresentadas, cinema ou vídeo', '15/peça', '2018-02-12 16:34:43', '2018-02-12 16:34:43'),
(55, 2, 'Partitura editada', '20/publicação', '2018-02-12 16:34:53', '2018-02-12 16:34:53'),
(56, 2, 'Coordenador de documentos cartográficos e mapas geológicos publicados', '25/documento', '2018-02-12 16:36:09', '2018-02-12 16:36:09'),
(57, 2, 'Coautor de documentos cartográficos e mapas geológicos publicados', '10/documento', '2018-02-12 16:36:21', '2018-02-12 16:36:21'),
(58, 2, 'Edição de rádio, cinema, vídeo ou televisão vinculada à atividade desenvolvida na UFBA', '10/atividade', '2018-02-12 16:36:31', '2018-02-12 16:36:31'),
(59, 2, 'Fotografia publicada', '2/foto', '2018-02-12 16:36:47', '2018-02-12 16:36:47'),
(60, 2, 'Patente examinada e concedida pelo INPI ou equivalente internacional', '25/patente', '2018-02-12 16:36:58', '2018-02-12 16:36:58'),
(61, 2, 'Desenho Industrial examinado e concedido pelo INPI ou equivalente internacional', '25/desenho industrial', '2018-02-12 16:37:09', '2018-02-12 16:37:09'),
(62, 2, 'Pedido de patente protocolado pela UFBA ou outra instituição no INPI ou equivalente internacional', '10/pedido', '2018-02-12 16:37:20', '2018-02-12 16:37:20'),
(63, 2, 'Pedido de Desenho Industrial protocolado pela UFBA ou outra instituição no INPI ou equivalente internacional', '10/pedido', '2018-02-12 16:37:40', '2018-02-12 16:37:40'),
(64, 2, 'Registro ou certificado de proteção de cultivar concedido pelo INPI ou equivalente internacional', '25/registro ou certificado', '2018-02-12 16:37:51', '2018-02-12 16:37:51'),
(65, 2, 'Registro ou certificado de proteção de cultivar protocolado pela UFBA ou outra instituição no INPI ou equivalente internacional', '10/registro ou certificado', '2018-02-12 16:38:04', '2018-02-12 16:38:04'),
(66, 2, 'Registro de marcas protocolados pela UFBA ou outra instituição no INPI ou equivalente internacional', '10/registro', '2018-02-12 16:38:15', '2018-02-12 16:38:15'),
(67, 2, 'Registro de softwares protocolados pela UFBA ou outra instituição no INPI ou equivalente internacional', '05/registro', '2018-02-12 16:38:24', '2018-02-12 16:38:24'),
(68, 2, 'Registro de software livre', '05/registro', '2018-02-12 16:38:35', '2018-02-12 16:38:35'),
(69, 3, 'Elaboração de projetos de extensão de caráter permanente ou temporário, com aprovação no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '02/atividade', '2018-02-12 16:41:31', '2018-02-12 16:41:31'),
(70, 3, 'Coordenação de programas/projetos de extensão registrados, com aprovação no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem (por projeto, mediante relatório atualizado)', '03/semestre', '2018-02-12 16:41:48', '2018-02-12 16:41:48'),
(71, 3, 'Relatório de programas/projetos de extensão registrado e aprovado no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '04/atividade', '2018-02-12 16:42:00', '2018-02-12 16:42:00'),
(72, 3, 'Participação em programas/projetos de extensão registrados, com aprovação no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem (por projeto, mediante relatório atualizado)', '01/semestre', '2018-02-12 16:42:12', '2018-02-12 16:42:12'),
(73, 3, 'Coordenação geral de congresso', '10/atividade', '2018-02-12 16:42:22', '2018-02-12 16:42:22'),
(74, 3, 'Coordenação de eventos (cursos de extensão CH < 8 h, jornadas, seminários, exposições, recitais e similares), registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '03/atividade', '2018-02-12 16:42:34', '2018-02-12 16:42:34'),
(75, 3, 'Membro de Comissão organizadora de congressos e outros eventos (cursos, jornadas, seminários, exposições, recitais e similares), registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '02/atividade', '2018-02-12 16:42:46', '2018-02-12 16:42:46'),
(76, 3, 'Coordenação de cursos (oficina, workshop, laboratório e treinamento, de caráter teórico e/ou prático, planejados e organizados de modo sistemático, com carga horária definida e processo de avaliação formal, além da frequência), com CH mínima 8 h e máxima até 180 h, registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '05/atividade', '2018-02-12 16:43:34', '2018-02-12 16:43:34'),
(77, 3, 'Coordenação de cursos de atualização registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '10/atividade', '2018-02-12 16:43:46', '2018-02-12 16:43:46'),
(78, 3, 'Coordenação de cursos de aperfeiçoamento e especialização registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '15/atividade', '2018-02-12 16:44:00', '2018-02-12 16:44:00'),
(79, 3, 'Ministrante de cursos (oficina, workshop, laboratório e treinamento, de caráter teórico e/ou prático, planejados e organizados de modo sistemático, com carga horária definida e processo de avaliação formal, além da frequência), com CH mínima 8 h e máxima até 180 h, registrados e aprovados no órgão de lotação do docente ou nas Unidades Universitárias em que se realizem', '01/15h de atividade', '2018-02-12 16:44:12', '2018-02-12 16:44:12'),
(80, 3, 'Prestação de serviços (consultorias, assessorias, cooperação técnica e institucional, assistência jurídica, assistência hospitalar e ambulatorial, perícias, laudos técnicos etc.), desde que aprovados pela instância de lotação do docente', '02/atividade', '2018-02-12 16:44:24', '2018-02-12 16:44:24'),
(81, 3, 'Trabalho de campo e/ou visita técnica, programas comunitários de mobilização interna e externa, entre outros de interesse da Instituição e da comunidade, que visam à produção e socialização de conhecimento, realizados junto a segmentos da sociedade, compreendendo diagnóstico, planejamento, treinamento e desenvolvimento de ações de forma participativa', '02/15h de atividade', '2018-02-12 16:44:35', '2018-02-12 16:44:35'),
(82, 3, 'Coordenação de ambientes de inovação (aceleradoras, pré-incubadoras, incubadora de empresas, parques tecnológicos), com relatório semestral aprovado pela COMPITEC', '1', '2018-02-12 16:44:45', '2018-02-12 16:44:45'),
(83, 3, 'Membro da equipe do Sistema Local de Inovação da UFBA, com apresentação de relatório semestral aprovado pela COMPITEC', '1', '2018-02-12 16:44:56', '2018-02-12 16:44:56'),
(84, 4, 'Obras, publicações e outros produtos acadêmicos premiados, na área de atuação do docente', '5', '2018-02-12 16:45:16', '2018-02-12 16:45:16'),
(85, 4, 'Obras, publicações e outros produtos acadêmicos premiados, fora da área de atuação do docente', '3', '2018-02-12 16:45:27', '2018-02-12 16:45:27'),
(86, 4, 'Comendas e premiações públicas de outra natureza', '2', '2018-02-12 16:45:37', '2018-02-12 16:45:37'),
(87, 5, 'Editor ou organizador de livro publicado (com ISBN), impresso ou meio digital, com circulação internacional', '15', '2018-02-12 16:45:52', '2018-02-12 16:45:52'),
(88, 5, 'Editor ou organizador de livro publicado (com ISBN) (impresso ou meio digital, com circulação nacional)', '10', '2018-02-12 16:46:02', '2018-02-12 16:46:02'),
(89, 5, 'Editor Chefe de Revista', '15', '2018-02-12 16:46:14', '2018-02-12 16:46:14'),
(90, 5, 'Editor Associado de Revista', '10', '2018-02-12 16:46:43', '2018-02-12 16:46:43'),
(91, 5, 'Membro de corpo editorial', '4', '2018-02-12 16:46:54', '2018-02-12 16:46:54'),
(92, 5, 'Revisor/parecerista de revista científica, de material didático, capítulo de livro', '4', '2018-02-12 16:47:07', '2018-02-12 16:47:07'),
(93, 5, 'Revisor de livros', '10', '2018-02-12 16:47:18', '2018-02-12 16:47:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `c1_3to14`
--

CREATE TABLE `c1_3to14` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_activity` int(10) UNSIGNED NOT NULL,
  `student` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `semester` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `bolsa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `agencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `c1_3to14`
--

INSERT INTO `c1_3to14` (`id`, `id_activity`, `student`, `semester`, `bolsa`, `agencia`, `titulo`, `created_at`, `updated_at`) VALUES
(2, 79, 'Tiago', '2018.1', 'nao', '-', '-', '2018-02-26 02:42:52', '2018-02-26 02:42:52'),
(3, 78, 'Fabio', '2017.2', 'nao', '-', '-', '2018-02-26 04:14:34', '2018-02-26 04:14:34'),
(4, 78, 'Fabiana Moraes', '2015.2', 'sim', 'naoseioquela', 'Tcc - 2', '2018-02-26 04:15:25', '2018-02-26 04:15:25'),
(5, 82, 'dsds', 'dsds', 'sim', 'dsds', 'dsd', '2018-02-26 04:38:43', '2018-02-26 04:38:43'),
(6, 84, 'Tiago', '20172', 'nao', '-', '-', '2018-02-27 04:50:32', '2018-02-27 04:50:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `create_report`
--

CREATE TABLE `create_report` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_register` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `comentary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `create_report`
--

INSERT INTO `create_report` (`id`, `id_register`, `id_user`, `comentary`, `created_at`, `updated_at`) VALUES
(13, 68, 4, 'fdfdfd', '2018-02-20 05:31:48', '2018-02-20 05:31:48'),
(14, 69, 4, 'dddddddddddddddd', '2018-02-20 05:32:00', '2018-02-20 05:32:00'),
(15, 72, 2, 'fdfwwwwwwwwwwwwwww', '2018-02-20 05:32:11', '2018-02-20 05:32:11'),
(16, 71, 2, 'fdfdfdfd', '2018-02-20 05:32:27', '2018-02-20 05:32:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estudante`
--

CREATE TABLE `estudante` (
  `idEstudante` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `register` varchar(45) NOT NULL,
  `course` varchar(45) NOT NULL,
  `departament` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_01_02_031907_edit_activity', 3),
(7, '2018_01_02_215204_edit_register', 5),
(8, '2018_01_02_221242_create_file', 6),
(10, '2018_01_02_204222_create_activity_teacher', 7),
(11, '2018_02_17_171901_create_relatorio_table', 8),
(12, '2018_02_25_025135_create_table_addforms3to14', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `register_activity`
--

CREATE TABLE `register_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` int(11) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_activity` int(10) UNSIGNED NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL,
  `anexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `register_activity`
--

INSERT INTO `register_activity` (`id`, `field`, `id_user`, `id_activity`, `description`, `score`, `year`, `anexo`, `observation`, `created_at`, `updated_at`) VALUES
(84, 1, 4, 4, 'Orientação de Tese de Doutorado defendida', 5, 2018, 'WCaznHhDuATildFlSkNs8FFxp7CjHlZdxdg7mfKX.pdf', 'nenhuma', '2018-02-27 04:50:32', '2018-02-27 04:50:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `register` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lattes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siape` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departament` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institution` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_admission_ufba` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `register`, `lattes`, `siape`, `departament`, `institution`, `date_admission_ufba`) VALUES
(1, 'Ivan', 'ivan@gmai.com', '$2y$10$7kiwFUb06wl2yK1iorpJiOIWDJHz4khJs3Un2EGpridP6rL86XjfS', 'ytfzq1OloV9r5KtTbwyTcE4ZGT8lJ054ey3rJTKpTwcqVuagRzAjBOPu0Rpr', '2018-01-01 18:54:42', '2018-01-01 18:54:42', '0', '0', '0', '0', '0', '0'),
(2, 'Tiago Doria', 'tiagodoriap@gmail.com', '$2y$10$QZvBE9jFlxEmdmZNk0bSlORv9Y5H4EOW2K2cWemIIeBNBScAhr/Ba', 'nzut3MHfmf7FEMtOeJKf4s2N3D0w3q8g99jvzEtrGBYitu7I9lcQfAnMI4zI', '2018-01-01 18:56:25', '2018-01-01 18:56:25', '0', '0', '0', '0', '0', '0'),
(3, 'Marcos', 'marcos@gmail.com', '$2y$10$2Fh4mGVAAk7IFKfl0nm46uefwvTAnDCGOJ.ebBczc6hkxyySwfhp.', 'q9tVs2wmXmTykauOdxYMSFmyO6fCFr3nHIjanFVDSiryXOm8zpMMsNKTDlXn', '2018-01-01 20:53:14', '2018-01-01 20:53:14', '0', '0', '0', '0', '0', '0'),
(4, 'Fred', 'fred@mail.com', '$2y$10$4oPNPGOrbu3EnV0nMwEMSObQKjwHj/y63GGnlRpeEvPCyryvn3hIG', 'zfoxTNYkLQg9rjp8qm9WmzZkqyzq1eMguCpP3oxdqRL0YiLeS9lLAlw0Ftoh', '2018-01-02 23:37:58', '2018-01-02 23:37:58', '212', 'asv', 'abc', 'dcc', 'ufba', '101093');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c1_3to14`
--
ALTER TABLE `c1_3to14`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c1_3to14_id_activity_foreign` (`id_activity`);

--
-- Indexes for table `create_report`
--
ALTER TABLE `create_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `create_report_id_register_foreign` (`id_register`),
  ADD KEY `create_report_id_user_foreign` (`id_user`);

--
-- Indexes for table `estudante`
--
ALTER TABLE `estudante`
  ADD PRIMARY KEY (`idEstudante`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `register_activity`
--
ALTER TABLE `register_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `register_activity_id_user_foreign` (`id_user`),
  ADD KEY `register_activity_id_activity_foreign` (`id_activity`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `c1_3to14`
--
ALTER TABLE `c1_3to14`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `create_report`
--
ALTER TABLE `create_report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `estudante`
--
ALTER TABLE `estudante`
  MODIFY `idEstudante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `register_activity`
--
ALTER TABLE `register_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `c1_3to14`
--
ALTER TABLE `c1_3to14`
  ADD CONSTRAINT `c1_3to14_id_activity_foreign` FOREIGN KEY (`id_activity`) REFERENCES `register_activity` (`id`);

--
-- Limitadores para a tabela `create_report`
--
ALTER TABLE `create_report`
  ADD CONSTRAINT `create_report_id_register_foreign` FOREIGN KEY (`id_register`) REFERENCES `register_activity` (`id`),
  ADD CONSTRAINT `create_report_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `register_activity`
--
ALTER TABLE `register_activity`
  ADD CONSTRAINT `register_activity_id_activity_foreign` FOREIGN KEY (`id_activity`) REFERENCES `activity` (`id`),
  ADD CONSTRAINT `register_activity_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
