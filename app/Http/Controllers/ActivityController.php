<?php

namespace App\Http\Controllers;
use App\Activity;
use App\User;
use App\Option;
use App\Register_activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFil;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Response;

class ActivityController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }


  public function view_details($id){

      $register = DB::table('register_activity')
        ->select('register_activity.*')
        ->where('register_activity.id',$id)
        ->first();

      $users = DB::table('users')
        ->select('users.*')
        ->where('users.id',$register->id_user)
        ->first();

      return view('view_detail')->with('register',$register)->with('users',$users);

  }

  

  public function view_activity(){
      $register = DB::table('register_activity')
        ->select('register_activity.*')
        ->where('register_activity.id_user',Auth()->user()->id)
        ->get();

      return view('view_activity',compact('register'));

  }


  public function ajax_select(Request $request){

      if($request->ajax()){

        $subcategories = DB::table('activity')->where('field',$request->cat_id)->get();
        return response()->json($subcategories);

      }

  }


   public function download_edital(){
      $anexo = 'edital_resolucao_ufba.pdf';
      $file = public_path()."\\".'download'."\\".$anexo;
      $headers = array(
          'Content-Type: application/pdf',
      );

      return Response::download($file, $anexo, $headers);

  }



} // fim classe
