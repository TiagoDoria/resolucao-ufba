<?php

namespace App\Http\Controllers;
use App\Activity;
use App\User;
use App\Create_report;
use App\Register_activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFil;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Response;
use PDF;

class AdminController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }


    public function register_activity(Request $request){

	    $exist = DB::table('activity')
	    ->select('activity.description')->where('activity.description',$request->description)
	    ->count();

	    $activity = new Activity;
	    $activity->field = $request->field;
	    $activity->description = $request->description;
	    $activity->pontos = $request->pontos;

	    if($exist == 0)
	        $activity->save();

	    return Redirect('register_activity');

    }


    public function download_file($id,$id2){

  		$anexo = DB::table('register_activity')
	      ->select('register_activity.anexo')->where('register_activity.id_user',$id2)
	      ->where('register_activity.id', $id)
	      ->first();
	    $file = storage_path(). "\\"  . "app" . "\\" . "public" . "\\" . $id2 . "\\" . $anexo->anexo;
	    $headers = array(
	              'Content-Type: application/pdf',
	    );

    	return Response::download($file, $anexo->anexo, $headers);

    }


    public function show_activity(){

	    $activity = DB::table('activity')
	    ->select('activity.*')->where('activity.field',1)
	    ->get();

  		return view('admin.show_activity',compact('activity'));

    }


  	public function view($id){

	    $register = DB::table('register_activity')
	      ->join('users','register_activity.id_user','=','users.id')
	      ->select('register_activity.*','users.name')
	      ->where('register_activity.id',$id)
	      ->get();

   		return view('admin.view',compact('register'));
    }


  	public function create_report(){

	    $register = DB::table('register_activity')
	      ->join('users','register_activity.id_user','=','users.id')
	      ->select('register_activity.*','users.name')
	      ->where('register_activity.status',1)
	      ->get();

    	return view('admin.list_registers')->with('register',$register);
    }


    public function analisados(){

	    $register = DB::table('register_activity')
	      ->join('users','register_activity.id_user','=','users.id')
	      ->select('register_activity.*','users.name')
	      ->where('register_activity.status',0)
	      ->get();

    	return view('admin.analisados')->with('register',$register);
    }


    public function confirmar_relatorio($id){

	    $register = DB::table('register_activity')
	      ->select('register_activity.*')
	      ->where('register_activity.id',$id)
	      ->first();

    	return view('admin.comentary')->with('register',$register);
    }


    public function send_comentary(Request $request){

    	$register = DB::table('register_activity')
	      ->select('register_activity.id_user')
	      ->where('register_activity.id',$request->id_activity)
	      ->first();

    	$report = new Create_report;
    	$report->id_register = $request->id_activity;
    	$report->id_user = $register->id_user;
    	$report->comentary = $request->comentary;
    	$report->save();

    	

    	DB::table('Register_activity')
            ->where('id', $request->id_activity)
            ->update(['status' => 0]);

    	return Redirect('pendentes');

    }


    public function perfil(){

   	 	$perfil = DB::table('users')
     	 ->select('users.*')->where('users.id',Auth()->user()->id)
      	 ->get();

    	return view('admin.perfil',compact('perfil'));  
    }


    public function perfil_professor($id){

   	 	$perfil = DB::table('users')
     	 ->select('users.*')->where('users.id',decrypt($id))
      	 ->get();

    	return view('admin.perfil_professor',compact('perfil'));  
    }


    public function ver_relatorios(){


   	 	$reports = DB::table('users')
	      ->join('create_report','create_report.id_user','=','users.id')
	      ->select('users.name','users.register') 
	      ->groupBy('name')
	      ->get();



    	return view('admin.list_reports',compact('reports','register'));  
    }


    public function create_report_pdf($id){

    	$reports = DB::table('register_activity')
	      ->join('users','register_activity.id_user','=','users.id')
	      ->join('create_report','register_activity.id','=','create_report.id_register')
	      ->select('register_activity.*','users.name','create_report.id_register')
	      ->where('register_activity.id',$id)
	      ->first();

	    $users = DB::table('users')
	      ->select('users.*')
	      ->where('users.id',$reports->id_user)
	      ->first();

	       $pdf = PDF::loadView('admin.relatorio',compact('reports','users'));
    		return $pdf->stream();

    }


} // fim classe
