<?php

namespace App\Http\Controllers;
use App\Activity;
use App\User;
use App\C1_3to14;
use App\Register_activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFil;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Response;

class TeacherController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }


  public function download($id){
      $anexo = DB::table('register_activity')
        ->select('register_activity.anexo')->where('register_activity.id_user',Auth()->user()->id)
        ->where('register_activity.id', $id)
        ->first();
      $file = storage_path(). "\\"  . "app" . "\\" . "public" . "\\" . Auth()->user()->id . "\\" . $anexo->anexo;
      $headers = array(
          'Content-Type: application/pdf',
      );

      return Response::download($file, $anexo->anexo, $headers);

  }

 


  public function create_pretension(Request $request){

      $pretension = new Pretension;
      $pretension->id_user = Auth()->user()->id;
      $pretension->pretension = $request->pretension;
      $pretension->save();

      return Redirect('home');

  }


  public function register(Request $request){

     
      $year = date("Y");
      $file = $request->file('anexo')->store(Auth()->user()->id);


      $act2 = DB::table('activity')
        ->select('activity.pontos','activity.id')->where('activity.description',$request->description)
        ->first();

      $score = $act2->pontos;
      $activity = Register_activity::create([
                  'field' => $request->field,
                  'id_user' => Auth()->user()->id,
                  'id_activity' =>$act2->id,
                  'description' => $request->description,
                  'score' => $score,
                  'anexo' => substr($file,2,strlen ($file)),
                  'observation' => $request->observation,
                  'year' => $year,
              ]);

   
      $activity->save();

      $act = DB::table('register_activity')
        ->select('register_activity.id')->where('register_activity.description',$request->description)
        ->first();

      $add = new C1_3to14();

      if($request->student){
        $add->id_activity = $act->id;
        $add->student = $request->student;
        $add->semester = $request->semester;
        $add->bolsa = $request->bolsa;
        if($add->bolsa == 'sim'){
          $add->agencia = $request->agencia;
          $add->titulo = $request->titulo;
        }
        $add->save();
      }
   
     return Redirect('home');

  }


  public function my_activity(){

    $activity = DB::table('register_activity')
    ->join('users','register_activity.id_user','=','users.id')
    ->join('activity','register_activity.id_activity','=','activity.id')
    ->select('register_activity.*','activity.pontos')
    ->where('register_activity.id_user',Auth()->user()->id)
    ->get();

    return view('my_activity',compact('activity'));

  }

  public function perfil(){

    $perfil = DB::table('users')
      ->select('users.*')->where('users.id',Auth()->user()->id)
      ->get();

    return view('perfil',compact('perfil'));  
  }

  public function edit(){
     $pessoais = DB::table('users')
        ->select('users.*')->where('users.id',Auth()->user()->id)
        ->first();

     return view('editar',compact('pessoais'));   
  }

  public function update(Request $request){
      $data = [
                  'name' => $request->name,
                  'email' => $request->email,
                  'register' =>$request->register,
                  'lattes' => $request->lattes,
                  'siape' => $request->siape,
                  'departament' => $request->departament,
                  'institution' => $request->institution
                 
              ];
      $expr = DB::table('users')
               ->select('users.*')->where('users.id',Auth()->user()->id)
               ->update($data);        

      return Redirect('meuperfil');

  }


  public function view_detail($id){
    $existe = DB::table('c1_3to14')
      ->select('c1_3to14.*')
      ->where('c1_3to14.id_activity',$id)
      ->first();

      

    if($existe) { 
        $aux = 'true';
        $activity = DB::table('register_activity')
          ->join('users','register_activity.id_user','=','users.id')
          ->join('activity','register_activity.id_activity','=','activity.id')
          ->join('c1_3to14','register_activity.id','=','c1_3to14.id_activity')
          ->select('register_activity.*','activity.*','users.name','c1_3to14.*')
          ->where('register_activity.id',$id)
          ->get();  
    }
    
    else{
       $aux = 'false';
       $activity = DB::table('register_activity')
          ->join('users','register_activity.id_user','=','users.id')
          ->join('activity','register_activity.id_activity','=','activity.id')
          ->select('register_activity.*','activity.*','users.name')
          ->where('register_activity.id',$id)
          ->get();  
    }      
    return view('view_detail',compact('activity','aux'));  
  }

} // fim classe
