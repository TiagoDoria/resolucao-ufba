<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class C1_3to14 extends Model
{
    use Notifiable;
    protected $table = 'c1_3to14';
    protected $fillable = [
        'id_activity', 'student', 'semester','bolsa', 'agencia', 'titulo'
    ];
}
