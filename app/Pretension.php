<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Pretension extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'pretension';
  protected $fillable = [
      'id_user','pretension'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
}
