<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Register_activity extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'register_activity';
  protected $fillable = [
      'field','id_user','id_activity', 'description','anexo','observation','score','year','status'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
}
