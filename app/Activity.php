<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Activity extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'activity';
  protected $fillable = [
      'field','description', 'pontos'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
}
