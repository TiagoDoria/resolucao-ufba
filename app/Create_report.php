<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Create_report extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'create_report';
  protected $fillable = [
      'id_register','comentary'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
}
